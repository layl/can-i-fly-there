import { createElement } from "react";
import ReactDOM from "react-dom";

import PopupApp from "~/popup/PopupApp";

const domContainer = document.querySelector('#popup');
ReactDOM.render(createElement(PopupApp), domContainer);
