import browser from "webextension-polyfill";

export type IcaoEntry = {
    status: "exists" | "renamed" | "missing";
    simIcao?: string;
}

export type StorageData = {
    icaoEntries: Record<string, IcaoEntry>;
    availableAircrafts: Record<string, string>;
    enableIcaos: boolean;
    enableAircrafts: boolean;
    enableCopyCoordinates: boolean;
}

export async function getStorage(): Promise<StorageData> {
    const storage = await browser.storage.local.get();

    if (storage.icaoEntries == undefined) {
        storage.icaoEntries = {};
    }

    if (storage.availableAircrafts == undefined) {
        storage.availableAircrafts = {};
    }

    if (storage.enableIcaos == undefined) {
        storage.enableIcaos = true;
    }

    if (storage.enableAircrafts == undefined) {
        storage.enableAircrafts = true;
    }

    if (storage.enableCopyCoordinates == undefined) {
        storage.enableCopyCoordinates = true;
    }

    return storage;
}