import browser from "webextension-polyfill";
import { getStorage } from "~storage";

async function onFeaturesSubmit(event: Event) {
    event.preventDefault();

    const icaosCheckbox = document.querySelector<HTMLInputElement>("#icaos-checkbox")!;
    const aircraftsCheckbox = document.querySelector<HTMLInputElement>("#aircrafts-checkbox")!;
    const copyCoordinatesCheckbox = document.querySelector<HTMLInputElement>("#copy-coordinates-checkbox")!;

    let storage = await getStorage();

    storage.enableIcaos = icaosCheckbox.checked;
    storage.enableAircrafts = aircraftsCheckbox.checked;
    storage.enableCopyCoordinates = copyCoordinatesCheckbox.checked;

    browser.storage.local.set(storage);
}

async function onLoaded() {
    const storage = await getStorage();
    document.querySelector<HTMLInputElement>("#icaos-checkbox")!.checked = storage.enableIcaos;
    document.querySelector<HTMLInputElement>("#aircrafts-checkbox")!.checked = storage.enableAircrafts;
    document.querySelector<HTMLInputElement>("#copy-coordinates-checkbox")!.checked = storage.enableCopyCoordinates;

    document.querySelector<HTMLFormElement>("#features-form")!.addEventListener("submit", onFeaturesSubmit);
}

document.addEventListener("DOMContentLoaded", onLoaded);
