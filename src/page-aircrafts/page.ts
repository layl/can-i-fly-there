import browser from "webextension-polyfill";
import { getStorage } from "~storage";

function onImportSubmit(event: any) {
    event.preventDefault();

    const fileField = document.querySelector<HTMLInputElement>("#import-file");

    const file = fileField!.files![0];
    if (!file) {
        return;
    }

    const reader = new FileReader();
    reader.onload = function(event) {
        const contents = event.target!.result as string;

        const availableAircrafts = JSON.parse(contents);
        browser.storage.local.set({ availableAircrafts: availableAircrafts });

        refreshList();

        alert("Imported " + Object.keys(availableAircrafts).length + " Aircrafts");
    };
    reader.readAsText(file);
}

async function onAddSubmit(event: any) {
    event.preventDefault();

    const fseNameField = document.querySelector<HTMLInputElement>("#add-name-fse")!;
    const simNameField = document.querySelector<HTMLInputElement>("#add-name-sim")!;

    const availableAircrafts = (await getStorage()).availableAircrafts;
    availableAircrafts[fseNameField.value] = simNameField.value;
    browser.storage.local.set({ availableAircrafts: availableAircrafts });

    refreshList();
}

async function refreshList() {
    const storage = await getStorage();

    const newList = document.createElement("ul");
    newList.id = "list";

    for (const key in storage.availableAircrafts) {
        const item = document.createElement("li");
        item.innerText = key + ": " + storage.availableAircrafts[key] + " ";

        const deleteButton = document.createElement("button");
        deleteButton.innerText = "Delete";
        deleteButton.onclick = async () => {
            const availableAircrafts = (await getStorage()).availableAircrafts;
            delete availableAircrafts[key];
            browser.storage.local.set({ availableAircrafts: availableAircrafts });

            refreshList();
        };
        item.appendChild(deleteButton);

        newList.appendChild(item);
    }

    const list = document.querySelector("#list")!;
    list.replaceWith(newList);
}

function onLoaded() {
    document.querySelector("#import-form")!.addEventListener("submit", onImportSubmit);
    document.querySelector("#add-form")!.addEventListener("submit", onAddSubmit);

    refreshList();
}

document.addEventListener("DOMContentLoaded", onLoaded);
