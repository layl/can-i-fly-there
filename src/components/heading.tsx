import React, { FunctionComponent } from "react";
import { createUseStyles } from "react-jss"

const useStyles = createUseStyles({
    heading: {
        fontSize: "1.25rem",
        marginBottom: "0.5rem",
    },
});

const Heading: FunctionComponent<{}> = (props) => {
    const classes = useStyles();

    return (
        <div className={classes.heading}>{props.children}</div>
    );
}

export default Heading;
