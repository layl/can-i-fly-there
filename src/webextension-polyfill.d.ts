type BrowserStorage = {
    local: any;
}

type BrowserDownloads = {
    download(options: any)
}

type Browser = {
    storage: BrowserStorage;
    downloads: BrowserDownloads;
    tabs: any;
}

const browser: Browser;

export default browser;